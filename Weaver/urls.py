from django.conf.urls import patterns, include, url
from newapp.api.resources import *
from tastypie.api import Api
from django.contrib import admin
from newapp.views import *

admin.autodiscover()
v1_api = Api(api_name='v1')
v1_api.register(apimapping())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Weaver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^$',hello),
    url(r'^api/', include(v1_api.urls)),
)
