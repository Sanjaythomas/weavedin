from tastypie.resources import ModelResource,Resource
from newapp.models import applicationmap
from django.db import models

class BaseCorsResource(Resource):
    """
    Class implementing CORS
    """
    def create_response(self, *args, **kwargs):
        response = super(BaseCorsResource, self).create_response(*args, **kwargs)
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    def method_check(self, request, allowed=None):
        if allowed is None:
            allowed = []

        request_method = request.method.lower()
        allows = ','.join(map(str.upper, allowed))

        if request_method == 'options':
            response = HttpResponse(allows)
            response['Access-Control-Allow-Origin'] = '*'
            response['Access-Control-Allow-Headers'] = 'Content-Type'
            response['Allow'] = allows
            raise ImmediateHttpResponse(response=response)

        if not request_method in allowed:
            response = http.HttpMethodNotAllowed(allows)
            response['Allow'] = allows
            raise ImmediateHttpResponse(response=response)

        return request_method

class BaseModelResource(BaseCorsResource, ModelResource):
    """
    Abstract class sample with template data for the models
    """

    class Meta:
        abstract = True
        excludes = ['creation_time', 'modification_time', 'deleted']
        list_allowed_methods = ['get', 'post', 'put']
        detail_allowed_methods = ['get', 'post', 'put']

class apimapping(BaseModelResource):
    class Meta:
        queryset = applicationmap.objects.all()
        resource_name = 'entry'
        allowed_methods = ['get','post']
        excludes=['emailid','password','id']
    def obj_get_list(self, bundle, **kwargs):
        try:
            email = bundle.request.GET['email']
            passw = bundle.request.GET['pass']
        except:
            email =""
            passw =""
        return super(apimapping, self).get_object_list(bundle).filter(emailid=email).filter(password=passw)