from django.db import models   
from tastypie.models import create_api_key 
# Create your models here.

class applicationmap(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=100)
    emailid = models.CharField(max_length=50)
    password = models.CharField(max_length=100)
    storename = models.CharField(max_length=50)
    storeurl = models.CharField(max_length=200)
    api_key = models.CharField(max_length=200)

    def __str__(self):
        return self.storeurl